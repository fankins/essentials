   let db;
        let currentDirId = 0; // 0 означает корневой уровень
        let dirPath = []; // Для хлебных крошек

        async function initDb() {
            const config = {
                locateFile: filename => `https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.8.0/${filename}`
            };
            const SQL = await initSqlJs(config);

            try {
                // Загружаем зашифрованный файл
                const response = await fetch(dburl);
                if (!response.ok) throw new Error('Не удалось загрузить базу данных');
                const encryptedText = await response.text();

                // Расшифровываем
                const decrypted = CryptoJS.AES.decrypt(encryptedText, encryptionKey);
                const decryptedBase64 = decrypted.toString(CryptoJS.enc.Utf8);

                if (!decryptedBase64) {
                    throw new Error('Ошибка расшифровки: неверный ключ или повреждённый файл');
                }

                // Преобразуем base64 обратно в бинарные данные
                const dbData = Uint8Array.from(atob(decryptedBase64), c => c.charCodeAt(0));
                db = new SQL.Database(dbData);

                console.log('База данных успешно расшифрована и загружена');
            } catch (error) {
                console.error('Ошибка:', error);
            }

            loadDirectory(currentDirId);
        }

        // Загрузка содержимого директории
        function loadDirectory(dirId) {
            currentDirId = dirId;
            const dirList = document.getElementById('dirList');
            const vaultBody = document.getElementById('vaultBody');
            const currentDirTitle = document.getElementById('currentDirTitle');

            // Устанавливаем заголовок
            if (dirId === 0) {
                currentDirTitle.textContent = 'Корневые директории';
            } else {
                const dir = db.exec("SELECT title FROM dir WHERE id = ?", [dirId])[0].values[0];
                currentDirTitle.textContent = dir[0];
            }

            // Загружаем поддиректории
            const dirs = db.exec("SELECT * FROM dir WHERE parent = ? OR (parent IS NULL AND ? = 0)", [dirId, dirId])[0];
            dirList.innerHTML = '';
            if (dirs) {
                dirs.values.forEach(dir => {
                    const li = document.createElement('li');
                    li.innerHTML = `<a href="#" onclick="loadDirectory(${dir[0]})">${dir[1]}</a>`;
                    dirList.appendChild(li);
                });
            }

            // Загружаем пароли
            const vaults = db.exec("SELECT * FROM vault WHERE dir_id = ?", [dirId])[0];
            vaultBody.innerHTML = '';
            if (vaults) {
                vaults.values.forEach(vault => {
                    const tr = document.createElement('tr');
                    tr.innerHTML = `
                        <td>${vault[1]}</td>
                        <td>${vault[2] || ''}</td>
                        <td>${vault[3] || ''}</td>
                        <td>${vault[4] || ''}</td>
                        <td>${vault[5] || ''}</td>
                    `;
                    vaultBody.appendChild(tr);
                });
            }

            updateBreadcrumbs();
            hideForms();
        }

        // Обновление хлебных крошек
        function updateBreadcrumbs() {
            const breadcrumbs = document.getElementById('breadcrumbs');
            breadcrumbs.innerHTML = '<a href="#" onclick="loadDirectory(0)">Главная</a>';

            if (currentDirId !== 0) {
                // Строим путь от текущей директории до корня
                let path = [];
                let parentId = currentDirId;
                while (parentId) {
                    const dir = db.exec("SELECT id, title, parent FROM dir WHERE id = ?", [parentId])[0].values[0];
                    path.unshift(dir);
                    parentId = dir[2]; // parent
                }

                path.forEach(dir => {
                    breadcrumbs.innerHTML += ` > <a href="#" onclick="loadDirectory(${dir[0]})">${dir[1]}</a>`;
                });
            }
        }

        // Показать формы
        function showAddDirForm() {
            document.getElementById('addDirForm').style.display = 'block';
            document.getElementById('addVaultForm').style.display = 'none';
        }

        function showAddVaultForm() {
            document.getElementById('addVaultForm').style.display = 'block';
            document.getElementById('addDirForm').style.display = 'none';
        }

        function hideForms() {
            document.getElementById('addDirForm').style.display = 'none';
            document.getElementById('addVaultForm').style.display = 'none';
        }


        // Инициализация
        initDb();
var canvas = document.getElementById("signature"),
    context = canvas.getContext("2d"),
    mouse = {x:0, y:0},
    draw = false;

context.fillStyle = "rgba(255, 255, 255, 0.05)";
context.fillRect(0, 0, canvas.width, canvas.height);
context.lineWidth = 2;

['mousedown', 'touchstart'].forEach(function (eventName) { // касание экране, начало рисования
    canvas.addEventListener(eventName, function(e){
        e.preventDefault();
        draw = true;
        mouse = {
            x: ((eventName === 'mousedown') ? e.pageX : e.touches[0].pageX) - this.offsetLeft,
            y: ((eventName === 'mousedown') ? e.pageY : e.touches[0].pageY) - this.offsetTop
        }
        context.beginPath();
        context.moveTo(mouse.x, mouse.y);
        context.lineTo(mouse.x+2, mouse.y+2);
        context.stroke();
    });
});

['mousemove', 'touchmove'].forEach(function (eventName) { // движение мыши/пальца
    canvas.addEventListener(eventName, function(e){
        e.preventDefault();
        if (draw === true){
            mouse = {
                x: ((eventName === 'mousemove') ? e.pageX : e.touches[0].pageX) - this.offsetLeft,
                y: ((eventName === 'mousemove') ? e.pageY : e.touches[0].pageY) - this.offsetTop
            }
            context.lineTo(mouse.x, mouse.y);
            context.stroke();
        }
    });
});

['mouseup', 'touchend'].forEach(function (eventName) { // окончание
    canvas.addEventListener(eventName, function (e) {
        e.preventDefault();
        if (eventName === 'mouseup') {
            mouse = {
                x: e.pageX - this.offsetLeft,
                y: e.pageY - this.offsetTop
            }
            context.lineTo(mouse.x, mouse.y);
            context.stroke();
        }
        context.closePath();
        draw = false;
    });
});

document.getElementById('signature-save').addEventListener('click', function () {
    document.getElementById("s-panel").style.display = 'none';
    document.getElementById("signature-result").innerHTML = '<img src="'+document.getElementById("signature").toDataURL('image/png')+'"/>';
});


function openS(){
    document.getElementById("s-panel").style.display = 'block';
}

document.designMode = "on";